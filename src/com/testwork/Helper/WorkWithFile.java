package com.testwork.Helper;

import org.junit.Test;

import java.io.*;

public class WorkWithFile {

    public void creatingAndWritingIntoNewFile(String fileName, String fileCompatible) throws IOException {

        File filePath = new File("C:\\Users\\Bender\\IdeaProjects\\testWork\\src\\com\\testwork\\Resource");
        filePath.mkdir();
        File file = new File(filePath + "\\" + fileName + ".txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileWriter writer = new FileWriter(file);
        writer.write(fileCompatible);
        writer.flush();
        writer.close();
    }





}
