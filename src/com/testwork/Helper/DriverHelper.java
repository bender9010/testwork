package com.testwork.Helper;

import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DriverHelper {

    private static ChromeOptions options;


    /**
     * Создаём массив на случай, если в тесте будет несколько драйверов
     */
    private static List<WebDriver> drivers = new ArrayList<>();
    static {

        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        options.addArguments("disable-infobars");
        options.addArguments("--start-fullscreen");

        DriverHelper.options = options;
    }

    /**
     * Создаём драйвер
     *
     * @return возвращает драйвер
     */
    public static WebDriver create() {
        ChromeDriver driver = new ChromeDriver(options);
        drivers.add(driver);
        WebDriverRunner.setWebDriver(driver);

        return driver;
    }

    /**
     * Вытаскиваем текущий драйвер
     *
     * @return WebDriver текущий драйвер
     */
    public static WebDriver getCurrentDriver() {
        if (drivers.isEmpty()) {
            return null;
        }

        return drivers.get(drivers.size() - 1);
    }

    /**
     * Закрываем текущий драйвер
     */
    public static boolean closeDriver(WebDriver driver) {
        if (driver == null) {
            return false;
        }

        for (int i = 0, n = drivers.size(); i < n; i++) {
            WebDriver d = drivers.get(i);

            if (d.equals(driver)) {
                d.close();
                drivers.remove(i);

                return true;
            }
        }

        assert false;

        return false;
    }

    /**
     * Закрываем все драйверы (браузеры)
     */
    public static void closeAll() {
//    drivers.forEach(WebDriver::close);
//    drivers.forEach(DriverHelper::closeDriver);
        for (int i = 0, n = drivers.size(); i < n; i++) {
            WebDriver d = drivers.get(i);
            d.close();
//      drivers.remove(i);
        }
        drivers.clear();
    }

    public static void switchTo(WebDriver driver) {
        WebDriverRunner.setWebDriver(driver);
    }

    /**
     * Забираем текущий URL
     */
    public static String getCurrentUrl() {
        WebDriver driver = DriverHelper.getCurrentDriver();
        assert driver != null;
        return driver.getCurrentUrl();
    }
}
