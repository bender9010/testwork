package com.testwork.Base;

import com.testwork.Helper.DriverHelper;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;


public abstract class BaseTest {

    @BeforeMethod
    public void beforeMethod(ITestContext testContext) {
        DriverHelper.create();
    }

    @AfterMethod
    public void afterMethod(ITestContext testContext, ITestResult testResult) {
        DriverHelper.closeAll();
    }
}
