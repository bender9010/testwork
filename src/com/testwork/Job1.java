package com.testwork;

import com.google.common.collect.ImmutableList;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;



public class Job1 {

    /**
     * Сортируем числа по возрастанию
     */
    private void sortNumbersByAscending() throws FileNotFoundException {
        File scannedFile = new File
                ("C:\\Users\\Bender\\IdeaProjects\\testWork\\src\\com\\testwork\\Resource\\Numbers.txt");

        Scanner scannerFromFile = new Scanner(scannedFile);

        StringBuilder currentNumbersFromFileBuilder = new StringBuilder();
        while (scannerFromFile.hasNext())
            currentNumbersFromFileBuilder.append(scannerFromFile.nextLine());
        String currentNumbersFromFile = currentNumbersFromFileBuilder.toString();

        System.out.println("Текущий порядок цифр в файле: " + currentNumbersFromFile);

        int[] sortedNumbers = Arrays.stream(currentNumbersFromFile.split(","))
                .mapToInt(Integer::parseInt).toArray();
        Arrays.sort(sortedNumbers);
        System.out.println("Отсортированные числа по возростанию: " + Arrays.deepToString(new int[][]{sortedNumbers}));
        scannerFromFile.close();
    }

    /**
     * Сортируем числа по убыванию
     */
    private void sortNumbersByDescending() throws FileNotFoundException {
        File scannedFile = new File
                ("C:\\Users\\Bender\\IdeaProjects\\testWork\\src\\com\\testwork\\Resource\\Numbers.txt");
        Scanner scannerFromFile = new Scanner(scannedFile);

        StringBuilder currentNumbersFromFileBuilder = new StringBuilder();
        while (scannerFromFile.hasNext())
            currentNumbersFromFileBuilder.append(scannerFromFile.nextLine());
        String currentNumbersFromFile = currentNumbersFromFileBuilder.toString();

        System.out.println("Текущий порядок цифр из файле: " + currentNumbersFromFile);

        int[] numbersWithoutCommas = Arrays.stream(currentNumbersFromFile.split(","))
                .mapToInt(Integer::parseInt).toArray();
        Integer[] sortedNumbers = Arrays.stream(numbersWithoutCommas).boxed().toArray(Integer[]::new);
        Arrays.sort(sortedNumbers, Collections.reverseOrder());
        Collections.reverse(ImmutableList.of(sortedNumbers));
        System.out.println("Отсортированные числа по убыванию: " + Arrays.toString(sortedNumbers));
        scannerFromFile.close();
    }



    @Test
    public void runSortNumbersByAscending() throws FileNotFoundException {
        sortNumbersByAscending();
    }

    @Test
    public void runSortNumbersByDescending() throws FileNotFoundException {
        sortNumbersByDescending();

    }
}
