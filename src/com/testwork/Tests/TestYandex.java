package com.testwork.Tests;

import com.testwork.Base.BaseTest;
import com.testwork.Pages.Yandex.GeneralPage;
import com.testwork.Pages.Yandex.MarketPage;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static com.testwork.Parametrs.URL.YANDEX_RU;

public class TestYandex extends BaseTest {

    private GeneralPage generalPage;
    private MarketPage marketPage;

    public TestYandex() {

        this.generalPage = new GeneralPage();
        this.marketPage = new MarketPage();
    }

    @Test
    public void tes1() {

        String minimumPriceForPhone = "40000";
        String minimumPriceForHeadphones = "17000";
        String maximumPriceForHeadphones = "25000";


        open(YANDEX_RU);
        generalPage.clickLinkMarket();
        marketPage.clickLinkAllCategories();
        marketPage.clickLinkElectronik();
        marketPage.clickLinkMobilyPhone();
        marketPage.clickCheckBoxSamsung();
        marketPage.setMinimumPrice(minimumPriceForPhone);
        marketPage.waitApplyChangeFilter();
        String titlePhoneFromProductCard = marketPage.getTitleFromProductCard();
        System.out.println("Название первого телефона в разделе телефоны: " + titlePhoneFromProductCard);
        marketPage.clickProductCardFirstPhone();
        String titlePhoneFromProductPage = marketPage.getTitleFromProductPage();
        System.out.println("Название телефона в карточки товара: " + titlePhoneFromProductPage);
        assert titlePhoneFromProductCard.equals(titlePhoneFromProductPage);

        marketPage.clickLinkAllCategories();
        marketPage.clickLinkElectronik();
        marketPage.clickLinkHeadphones();
        marketPage.setMinimumPrice(minimumPriceForHeadphones);
        marketPage.setMaximumPrice(maximumPriceForHeadphones);
        marketPage.clickCheckBoxBeats();
        marketPage.waitApplyChangeFilter();
        String titleHeadphonesFromProductCard = marketPage.getTitleFromProductCard();
        System.out.println("Название первых наушников в разделе наушники: " + titleHeadphonesFromProductCard);
        marketPage.clickProductCardFirstPhone();
        String titleHeadphonesFromProductPage = marketPage.getTitleFromProductPage();
        System.out.println("Название наушников в карточки товара: " + titleHeadphonesFromProductPage);
        assert titleHeadphonesFromProductCard.equals(titleHeadphonesFromProductPage);
    }

}
