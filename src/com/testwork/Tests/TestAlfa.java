package com.testwork.Tests;

import com.testwork.Base.BaseTest;
import com.testwork.Helper.WorkWithFile;
import com.testwork.Pages.AlfaBank.JobsPage;
import com.testwork.Pages.Google.ResultsSearchPage;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Date;

import static com.codeborne.selenide.Selenide.open;
import static com.testwork.Parametrs.URL.GOOGLE_COM;

public class TestAlfa extends BaseTest {

    private com.testwork.Pages.Google.GeneralPage generalGoogle;
    private com.testwork.Pages.AlfaBank.GeneralPage  generalAlfa;
    private ResultsSearchPage resultsSearchPage;
    private JobsPage jobsPage;
    private WorkWithFile workWithFile;

    public TestAlfa() {

        this.generalAlfa = new com.testwork.Pages.AlfaBank.GeneralPage();
        this.generalGoogle = new com.testwork.Pages.Google.GeneralPage();
        this.resultsSearchPage = new ResultsSearchPage();
        this.jobsPage = new JobsPage();
        this.workWithFile = new WorkWithFile();
    }

    @Test
    public void test2() throws IOException {
        Date date = new Date();
        String defaultCurrentDate = date.toString();
        String currentDate = defaultCurrentDate.replaceAll(":", "-");

        String question = "Альфа банк";

        open(GOOGLE_COM);
        generalGoogle.setQuestionIntoSearchField(question);
        generalGoogle.clickButtonSearch();
        resultsSearchPage.clickFirstResultSearch();
        generalAlfa.clickJobsLink();
        String aboutCompany = jobsPage.aboutCompany();
        workWithFile.creatingAndWritingIntoNewFile(currentDate, aboutCompany);
    }
}
