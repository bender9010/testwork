package com.testwork.Pages.Yandex;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class GeneralPage {

    private SelenideElement getLinkMarket(){
        return $("[data-id=\"market\"]").shouldBe(Condition.visible);
    }

    public void clickLinkMarket() {
        getLinkMarket().click();
    }
}
