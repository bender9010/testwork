package com.testwork.Pages.Yandex;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class MarketPage {

    private SelenideElement getLinkAllCategories() {
        return $("[class*=\"horizontal-tabs\"] [class*=\"navigation-menu-grouping\"]")
                .shouldBe(Condition.visible);
    }

    private SelenideElement getLinkElectronik() {
        return $("[class*=\"vertical-tabs\"] > div:first-child").shouldBe(Condition.visible);
    }

    private SelenideElement getLinkMobilyPhone() {
        return $("[href*=\"mobilnye-telefony\"]").shouldBe(Condition.visible);
    }

    private SelenideElement getLinkHeadphones() {
        return $("[href*=\"naushniki\"]").shouldBe(visible);
    }

    private SelenideElement getFieldMinimumPrice() {
        return $("#glpricefrom").shouldBe(Condition.visible);
    }

    private SelenideElement getFieldMaximumPrice() {
        return $("#glpriceto").shouldBe(Condition.visible);
    }

    private SelenideElement getCheckBoxSamsung() {
        return $$(".NVoaOvqe58").find(Condition.text("Samsung"));
    }


    private SelenideElement getCheckBoxBeats() {
        return $$(".NVoaOvqe58").find(Condition.text("Beats"));
    }

    private SelenideElement getProductCardFirstPhone() {
       return  $$("[data-id*=\"model\"]").get(0);
    }

    public String getTitleFromProductCard() {
        return getProductCardFirstPhone().find(By.cssSelector("[class*=\"title\"] a")).getText();
    }

    public String getTitleFromProductPage() {
        return $("[class^=\"n-title\"] .title").getText();
    }

    public void clickLinkAllCategories() {
        getLinkAllCategories().click();
    }

    public void clickLinkElectronik() {
        getLinkElectronik().click();
    }

    public void clickLinkMobilyPhone() {
        getLinkMobilyPhone().click();
    }

    public void clickLinkHeadphones() {
        getLinkHeadphones().click();
    }

    public void setMinimumPrice(String minimumPrice) {
        getFieldMinimumPrice().click();
        getFieldMinimumPrice().setValue(minimumPrice);
    }

    public void setMaximumPrice(String maximumPrice) {
        getFieldMaximumPrice().click();
        getFieldMaximumPrice().setValue(maximumPrice);
    }

    public void clickCheckBoxSamsung() {
        getCheckBoxSamsung().click();
    }

    public void clickCheckBoxBeats() {
        getCheckBoxBeats().click();
    }

    public void clickProductCardFirstPhone() {
        $$("[class*=\"title\"] a").get(0).click();
    }

    public void waitApplyChangeFilter() {
        sleep(1000);
        $("[class*=\"paranja\"]").waitUntil(disappear, 10000);
    }
}
