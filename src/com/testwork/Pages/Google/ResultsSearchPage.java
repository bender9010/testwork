package com.testwork.Pages.Google;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$$;

public class ResultsSearchPage {

    private SelenideElement getFirstResultSearch() {
        return $$(".r .LC20lb")
                .get(0)
                .shouldBe(Condition.visible);
    }

    public void clickFirstResultSearch() {
        getFirstResultSearch().click();
    }
}
