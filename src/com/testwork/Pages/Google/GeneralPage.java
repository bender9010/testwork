package com.testwork.Pages.Google;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class GeneralPage {

    private SelenideElement getSearchField() {
        return $("[title=\"Поиск\"]").shouldBe(Condition.visible);
    }

    private SelenideElement getButtonSearch() {
        return $("[jsname=\"VlcLAe\"] .gNO89b").shouldBe(Condition.visible);
    }

    public void setQuestionIntoSearchField(String question) {
        getSearchField().setValue(question);
    }

    public void clickButtonSearch() {
        getButtonSearch().click();
    }

}
