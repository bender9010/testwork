package com.testwork.Pages.AlfaBank;

import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;

public class JobsPage {

    private String firstStringAboutCompany() {
        return $$(".top-32 p:nth-child(1)").get(0).getText();
    }

    private String secondStringAboutCompany() {
        return $$(".top-32 p:nth-child(2)").get(0).getText();
    }

    private String thirdStringAboutCompany() {
        return $(".top-32 p:nth-child(3)").getText();
    }


    public String aboutCompany() {
        return firstStringAboutCompany()
                + "\n" + secondStringAboutCompany()
                + "\n" + thirdStringAboutCompany();
            }
}
