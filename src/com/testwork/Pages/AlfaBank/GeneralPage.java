package com.testwork.Pages.AlfaBank;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.JavascriptExecutor;

import static com.codeborne.selenide.Selenide.$;

public class GeneralPage {

    private SelenideElement getJobsLink() {
        return $("[title=\"Вакансии\"]").scrollTo();
    }

    public void clickJobsLink(){
        getJobsLink().click();
    }
}
